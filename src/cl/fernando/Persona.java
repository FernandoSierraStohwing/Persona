package cl.fernando;

/**
 *
 * @author CETECOM
 */
public class Persona {

    //1 crear atributos
    private String nombre, rut, apellido;
    private int edad;

    //2 crear constructor con y sin parametros

    public Persona() {
        System.out.println("hola, soy el constructor sin parametros");
    }

    public Persona(String nombre, String rut, String apellido, int edad) {
        this.nombre = nombre;
        this.rut = rut;
        this.apellido = apellido;
        this.edad = edad;
        System.out.println("hola, soy el constructor con parametros");
    }
    
    //3 crear accesadores y mutadores (getter y setter)

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    //4 creamos metodo toString

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", rut=" + rut + ", apellido=" + apellido + ", edad=" + edad + '}';
    }
    
    //5 creamos metodos customer
    public void respirar(){
        System.out.println("estoy respirando...");
    }
    
    public void comer(){
        System.out.println("estoy comiendo");
    }
    
    
    
}
