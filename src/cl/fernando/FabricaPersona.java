package cl.fernando;

/**
 *
 * @author CETECOM
 */
public class FabricaPersona {
    public static void main(String[] args) {
        //6.1 instanciamos objetos persona con parametros
        Persona persona1 = new Persona("Pablo", "17258963-5", "Viedma", 40);
        System.out.println(persona1);
        persona1.respirar();
        //6.2 instanciamos objeto persona sin parametros
        Persona persona2 = new Persona();
        //6.2.1 modificamos los atributos del objeto mediante el metodo set();
        persona2.setNombre("Cristobal");
        persona2.setEdad(33);
        persona2.setRut("123456789-0");
        System.out.println(persona2);
        
    }
    
    
}
